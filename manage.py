#!/usr/bin/env python3
from  flask.ext.script import Manager, Server
from main import app

manager = Manager(app)

@manager.shell
def make_shell_context():
    return dict(app=app)

if __name__=='__main__':
    manager.run()
